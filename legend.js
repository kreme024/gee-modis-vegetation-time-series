// intialize palette for map layer/legend visualization
var vegetation_palette = [
  'FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163', '99B718', '74A901',
  '66A000', '529400', '3E8601', '207401', '056201', '004C00', '023B01',
  '012E01', '011D01', '011301'
];

var legend_title =  function(title){
  return ui.Label({
    value: title,
    style: {
    fontWeight: 'bold',
    fontSize: '16px',
    position: 'top-center',
    textAlign: 'center',
    margin: '15px 0 4px 0',
    padding: '5px '
  }});
};


// Create the color bar for the legend.
var color_bar = function(palette, min, max){
  return ui.Thumbnail({
    image: ee.Image.pixelLonLat().select(0),
    params: {bbox: [0, 0, 1, 0.1], dimensions: '100x10', format: 'png', min: min, max: max, palette: palette},
    style: {stretch: 'horizontal', margin: '0px 8px', maxHeight: '24px'},
  });
};

// Create a panel with three numbers for the legend.
var legend_labels = function(min, max){
  return ui.Panel({
    widgets: [
      ui.Label(min, {margin: '4px 8px'}),
      ui.Label((max / 2), {margin: '4px 8px', textAlign: 'center', stretch: 'horizontal'}),
      ui.Label(max, {margin: '4px 8px'})
    ],
    layout: ui.Panel.Layout.flow('horizontal')
  });
};

exports.title = legend_title;
exports.labels = legend_labels;
exports.color_bar = color_bar;
exports.veg_color_bar = color_bar(vegetation_palette, 0, 1);
exports.palette = vegetation_palette;