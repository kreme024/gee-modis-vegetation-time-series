var legend = require("modis-vi-ts-chart:legend");
var palette = legend.palette;

var merge_and_sort_collections = function(image_collection1, image_collection2){
  var merged_collection = image_collection1.merge(image_collection2);
  return merged_collection.sort("system:time_start");
};

var modis_vi_combined = merge_and_sort_collections(
  ee.ImageCollection('MODIS/006/MOD13Q1'),
  ee.ImageCollection('MODIS/006/MYD13Q1')
);

var modis_vi = modis_vi_combined.map(function(image){
    var out_image = image.divide(10000);
    out_image = out_image.set({"system:time_start": image.get("system:time_start")});
    return out_image;
  }
);

var inv_modis_vi = modis_vi.sort("system:time_start", false);
var gpp_collection = ee.ImageCollection("MODIS/006/MOD17A2H");
var inv_gpp_collec = gpp_collection.sort("system:time_start", false);
var npp_collection = ee.ImageCollection('MODIS/006/MOD17A3HGF');
var inv_npp_collec = npp_collection.sort("system:time_start", false);
var lai_collection = ee.ImageCollection('MODIS/006/MCD15A3H');
var inv_lai_collec = lai_collection.sort("system:time_start", false);

var modis = {
  "NDVI": {
    "collection": modis_vi,
    "band": "NDVI",
    "start": modis_vi.first().get("system:time_start"),
    "end": inv_modis_vi.first().get("system:time_end"),
    "visual": modis_vi.filterDate("2019-01-01", "2020-01-01").select("NDVI").median(),
    "resolution": 250,
    "vis_params": {min: 0, max: 1, palette: palette},
    "chart_title": "MODIS Normalized Difference Vegetation Index Time-Series",
    "v_axis_title": "NDVI",
    "period": "16-Day",
    "min": 0.0,
    "max": 1.0,
    "aggregation": "avg",
  },
  "EVI": {
    "collection": modis_vi,
    "band": "EVI",
    "start": modis_vi.first().get("system:time_start"),
    "end": inv_modis_vi.first().get("system:time_end"),
    "visual": modis_vi.filterDate("2019-01-01", "2020-01-01").select("EVI").median(),
    "resolution": 250,
    "vis_params": {min: 0, max: 1, palette: palette},
    "chart_title": "MODIS Enhanced Vegetation Index Time-Series",
    "v_axis_title": "EVI",
    "period": "16-Day",
    "min": 0.0,
    "max": 1.0,
    "aggregation": "avg",
  },
  "GPP": {
    "collection": gpp_collection,
    "band": "Gpp",
    "start": gpp_collection.first().get("system:time_start"),
    "end": inv_gpp_collec.first().get("system:time_end"),
    "visual": gpp_collection.filterDate("2019-01-01", "2020-01-01").select("Gpp").sum(),
    "resolution": 500,
    "vis_params": {min: 0, max: 20000, palette: palette},
    "chart_title": "MODIS Gross Primary Production Time-Series",
    "v_axis_title": "GPP kg*C/m²",
    "period": "8-Day",
    "min": 0.0,
    "max": 20000.0,
    "aggregation": "sum",
  },
  "NPP": {
    "collection": npp_collection,
    "band": "Npp",
    "start": npp_collection.first().get("system:time_start"),
    "end": inv_npp_collec.first().get("system:time_end"),
    "visual": npp_collection.filterDate("2019-01-01", "2020-01-01").select("Npp").first(),
    "resolution": 500,
    "vis_params": {min: 0, max: 20000, palette: palette},
    "chart_title": "MODIS Net Primary Production Time-Series",
    "v_axis_title": "GPP kg*C/m²",
    "period": "Yearly",
    "min": 0.0,
    "max": 20000.0,
    "aggregation": "avg",
  },
  "LAI": {
    "collection": lai_collection,
    "band": "Lai",
    "start": lai_collection.first().get("system:time_start"),
    "end": inv_lai_collec.first().get("system:time_end"),
    "visual": lai_collection.filterDate("2019-01-01", "2020-01-01").select("Lai").median(),
    "resolution": 500,
    "vis_params": {min: 0, max: 100, palette: palette},
    "chart_title": "MODIS Leaf Area Index Time-Series",
    "v_axis_title": "LAI %",
    "period": "4-Day",
    "min": 0.0,
    "max": 100.0,
    "aggregation": "avg",
  }
};

exports.modis = modis;
