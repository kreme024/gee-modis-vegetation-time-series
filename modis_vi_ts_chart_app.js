var collections = require("modis-vi-ts-chart:collection_config");
var geometries = require("modis-vi-ts-chart:geometries");
var legend = require("modis-vi-ts-chart:legend");
var style = require("modis-vi-ts-chart:styles");

// 1. Define utility function to clean time-series dictionaries later-on
var drop_nulls_from_dict = function(dictionary){
  return dictionary.map(function(key, value){return dictionary.get(key)});
};

// 2. Initialize Map, control panel, and chart_panel
var band = "NDVI";
var data_layer = collections.modis[band];

var map_panel = ui.Map({"lat":15, "lon":0, "zoom":2});
var drawingTools = map_panel.drawingTools();
drawingTools.setShown(false);
map_panel.addLayer(data_layer.visual, data_layer.vis_params, band);

var control_panel = ui.Panel({style: {width: "290px"}});
control_panel.add(ui.Label({value: 'MODIS Vegetation Time-Series Charts', style: style.control.title}));

var chart_panel = ui.Panel({style: {width: "500px"}});
chart_panel.add(ui.Label({value: 'Follow the instructions on the left to create your time-series chart', style: style.control.title}));
var chart_options = style.chart.options[band];
var chart_panel_title = "MODIS NDVI Time-Series";

// 3. Create ui data layer selection tool
var options = ["NDVI","EVI","GPP","NPP","LAI"];
var layer_selector = ui.Select({
  items: options,
  placeholder: "Select Layer",
  onChange: function(value){
    data_layer = collections.modis[value];
    band = data_layer.band;
    chart_options = style.chart.options[band];
    chart_panel_title = "MODIS " + value + " Time-Series";
    // Replace active layer if different metric is chosen
    if (value != map_panel.layers().get(0).getName()){
      map_panel.clear();
      drawingTools.setShown(false);
      map_panel.addLayer(data_layer.visual, data_layer.vis_params, value);
      control_panel.widgets().set(10, legend.title("MODIS " + value + " 2019"));
      control_panel.widgets().set(12, legend.labels(data_layer.min, data_layer.max));
    }
  },
  style:{stretch: "horizontal"}
});
control_panel.add(ui.Label({value:'1. Select a metric of interest.', style:{padding:"15px 0 0 0"}}));
control_panel.add(layer_selector);

// 4. Create ui geometry drawing functionality
var geom_selector_1 = geometries.ui_selector(drawingTools, 0, "AOI1", "F5793A");
var geom_selector_2 = geometries.ui_selector(drawingTools, 1, "AOI2", "0F2080");
control_panel.add(ui.Label('2. Select a geometry type and place your first/reference geometry on the map.'));
control_panel.add(geom_selector_1);
control_panel.add(ui.Label('3. Select and place/draw your second geometry on the map.'));
control_panel.add(geom_selector_2);

// 5. Create chart
var show_ts_chart = ui.Button({label: "Show Time-series", onClick: function(){

  // store geometry objects as aois
  var aoi1 = drawingTools.layers().get(0).geometries().get(0);
  var aoi2 = drawingTools.layers().get(1).geometries().get(0);

  // get timestamps from the image collection and stack to single image
  var timestamps = data_layer.collection.aggregate_array("system:time_start");
  var date_strings = timestamps.map(function(timestamp){return ee.Date(timestamp).format("YYYY-MM-dd")});
  var stacked_image = data_layer.collection.select(band).toBands().rename(date_strings);

  // extract time-series to dictionaries
  var ts_aoi1 = ee.Image(stacked_image).reduceRegion({geometry: aoi1, reducer: ee.Reducer.mean(), scale: data_layer.resolution, bestEffort: true});
  var ts_aoi2 = ee.Image(stacked_image).reduceRegion({geometry: aoi2, reducer: ee.Reducer.mean(), scale: data_layer.resolution, bestEffort: true});
  ts_aoi1 = drop_nulls_from_dict(ts_aoi1);
  ts_aoi2 = drop_nulls_from_dict(ts_aoi2);

  // Time-series of aois can cover different dates - reduce to common dates for chart creation
  if(ts_aoi1.keys() != ts_aoi2.keys()){
    var in1_butnot2 = ts_aoi1.keys().removeAll(ts_aoi2.keys());
    var in2_butnot1 = ts_aoi2.keys().removeAll(ts_aoi1.keys());
    ts_aoi1 = ts_aoi1.remove(in1_butnot2);
    ts_aoi2 = ts_aoi2.remove(in2_butnot1);

    // Reassign timestamps to numerical date format for chart creation
    timestamps = ts_aoi1.keys().map(function(val){return(ee.Date.parse("YYYY-MM-dd", val).millis())});
  }

  // Concatenate the y-axis data by stacking both time-series on the 1-axis.
  var y_values = ee.Array.cat([ts_aoi1.values(), ts_aoi2.values()], 1);

  // Create the chart object
  var chart = ui.Chart.array.values(y_values, 0, ee.Array(timestamps))
    .setSeriesNames(["AOI1", "AOI2"])
    .setOptions(chart_options);

  // Show chart and reset control panel
  chart_panel.widgets().reset([ui.Label({value: chart_panel_title, style: style.control.title})]);
  chart_panel.add(chart);
  geom_selector_1.setValue(null, false);
  geom_selector_2.setValue(null, false);
},
  style: {stretch: "horizontal"}
});

// 6. Finalize control panel with legend
control_panel.add(ui.Label('4. Click "Show Time-Series" to view the comparison.'));
control_panel.add(show_ts_chart);
control_panel.add(ui.Label('5. Wait for chart to render and/or repeat steps 1-4 for another metric/location.'));
control_panel.add(legend.title("MODIS NDVI 2019"));
control_panel.add(legend.veg_color_bar);
control_panel.add(legend.labels(0, 1));

// 7. Start App
ui.root.clear();
ui.root.add(ui.SplitPanel(ui.Panel(ui.SplitPanel(control_panel, map_panel)), chart_panel));
