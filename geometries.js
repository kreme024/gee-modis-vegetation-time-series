var geometry_icons = {
  "polygon": '🔺',
  "point": '📍',
};

var options = [{"label": geometry_icons.point + ' Point', "value": "point"},
              {"label": geometry_icons.polygon + ' Polygon', "value": "polygon"}];

function reset_geometry_layers(drawingTools) {
  var layers = drawingTools.layers();
  layers.reset([]);
}

var select_and_draw_single_geometry = function(drawingTools, max_geom_layers, name, color){
  return ui.Select(
    {
      items: options,
      placeholder: 'Select Geometry Type',
      onChange: function(value){
        if(value){
          if(drawingTools.layers().length() > max_geom_layers){reset_geometry_layers(drawingTools)}
          var geom_layer = ui.Map.GeometryLayer({geometries: null, name: name, color: color});
          drawingTools.layers().add(geom_layer);
          drawingTools.onDraw(function(){drawingTools = drawingTools.stop()});
          drawingTools.setSelected(geom_layer);
          drawingTools.setShape(value);
          drawingTools.draw();
      }},
      style: {stretch: 'horizontal'}
    }
  );
};

exports.ui_selector = select_and_draw_single_geometry;