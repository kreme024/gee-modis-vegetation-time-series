var chart = {};

chart.panel = ui.Panel({
  style: {height: '250px', maxWidth: '700px', position: 'bottom-right', shown: false}
});

chart.options = {};

var series_options = {0: {color: "F5793A", lineWidth: 2}, 1: {color: "0F2080", lineWidth: 2}};
var pointSize = 3;
var chart_padding = '4px';
var chart_font_size = '4px';
var trendlines = {
  0: {
    type: 'linear', visibleInLegend: true,
    labelInLegend: 'Multi-year Trend',
    seriesName: 'multi-year series'
  },
  1: {
    type: 'linear', visibleInLegend: true,
    labelInLegend: 'Multi-year Trend',
    seriesName: 'multi-year series'
  }
};
var gpp_vaxis =  {title: "GPP kg*C / m²", minValue:0, viewWindow: {min: 0}};
var gpp_title = 'MODIS Gross Primary Production Time-Series';

chart.options["Gpp"] = {
  vAxis: gpp_vaxis,
  title: gpp_title,
  padding: chart_padding,
  fontSize: chart_font_size,
  series: series_options,
  trendlines: trendlines,
  pointSize: 0
};

var ndvi_vaxis =  {title: "NDVI", minValue:0, viewWindow: {min: 0}};
var ndvi_title = 'MODIS Normalized Difference Vegetation Index Time-Series'
chart.options["NDVI"] = {
  vAxis: ndvi_vaxis,
  title: ndvi_title,
  padding: chart_padding,
  fontSize: chart_font_size,
  series: series_options,
  trendlines: trendlines,
  pointSize: 0
};

var evi_vaxis =  {title: "EVI", minValue:0, viewWindow: {min: 0}};
var evi_title = 'MODIS Enhanced Vegetation Index Time-Series'
chart.options["EVI"] = {
  vAxis: evi_vaxis,
  title: evi_title,
  padding: chart_padding,
  fontSize: chart_font_size,
  series: series_options,
  trendlines: trendlines,
  pointSize: 0
};

var npp_vaxis =  {title: "NPP kg*C / m²", minValue:0, viewWindow: {min: 0}};
var npp_title = 'MODIS Net Primary Production Time-Series';

chart.options["Npp"] = {
  vAxis: npp_vaxis,
  title: npp_title,
  padding: chart_padding,
  fontSize: chart_font_size,
  series: series_options,
  trendlines: trendlines,
  pointSize: pointSize
};

var lai_vaxis =  {title: "LAI %", minValue:0, viewWindow: {min: 0}};
var lai_title = 'MODIS Leaf Area Index Time-Series';

chart.options["Lai"] = {
  vAxis: lai_vaxis,
  title: npp_title,
  padding: chart_padding,
  fontSize: chart_font_size,
  series: series_options,
  trendlines: trendlines,
  pointSize: 0
};

var control = {};

control.panel ={
  position: 'top-center',
  maxWidth: '250px',
  padding: '8px'
};

control.title =  {
  fontWeight: 'bold',
  fontSize: '16px',
  position: 'top-center',
  textAlign: 'center',
  margin: '0 0 4px 0',
  padding: '5px'
};



exports.chart = chart;
exports.control = control;

