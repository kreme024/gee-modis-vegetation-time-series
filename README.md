# CQuest's GEE MODIS VI TS Chart

This is the source code for the CQuest.ai MODIS vegetation index time-series chart app on https://cquest.earth/.

The code is provided in 5 separate javascript files on this repo.
You can use this code in the Google Earth Engine (GEE) Code Editor to recreate the app.

The core functionality of the app resides in modis_vi_ts_chart_app.js.
The other four scripts serve as supplementary reference files:
collection_config.js contains configuration details of all the relevant MODIS vegetation image collections on GEE,
geometries.js deals with the interactive drawing of geometries on the map,
legend.js provides the configuration for the map legend,
and styles.js holds the styling details for the user interface, i.e. control and chart panels.

For more information check out https://cquest.ai/ and https://cquest.earth/ or write an email to julian@cquest.ai
